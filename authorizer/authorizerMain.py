from urllib.error import HTTPError
from abc import ABC
import base64
import jwt
import json
import httpConstants as hc
import CheckEmptyUtils as emptyUtils
import os
import time
obj_hc = hc.HttpConstants()

class AuthorizerMain(ABC):
	ISS_OS_MONOLITH = "OurSCHOOL monolith"
	AUD_CONTENT_MOUNT_TOOL = "Content mount tool"
	ROLE_ID_CONTENT_MOUNT_TOOL_ACCESS = 42
	ROLE_ID_ADMIN = 1
	ALLOWED_ENVS = os.environ.get('ALLOWED_ENVS')
	#ALLOWED_ENVS = os.environ.get('AllowedEnvs')
	
	def entry(self, authorization_token):
		return self.authorization_filter(authorization_token)

	def authorization_filter(self, data):
		#print('In authorization_filter .......')
		auth_output = None

		try:
			if not isinstance(data, dict):
				data = json.loads(data)

			input_type = data['type']

			if input_type == "TOKEN":
				authorization_token = data['authorizationToken']

				# Split the token into the actual token and the index
				# Index comes from the time the token was created
				# See JWTUtil.java in the ttfm/monolith project.
				arr_auth_token = authorization_token.split('||')
				index = arr_auth_token[0]
				token = arr_auth_token[1]

				dict_decoded_token = self.decode_token(index, token)
				#print( json.dumps(dict_decoded_token, indent=4) )
				
				if dict_decoded_token is not None:
					#print(" Decoded Token ")
					dict_auth = self.authorize_app_user(dict_decoded_token)
					auth_code = dict_auth['authCode']
					principal_id = dict_auth['appUserId']

					if auth_code == hc.HttpConstants.HTTP_RESPONSE_OK:
						auth_output = 'Allow'
					else:
						auth_output = 'Deny'
						
				else:
					print( "ValueErr 1")
					raise ValueError

			elif input_type == "REQUEST":
				# code block for REQUEST type input
				pass
			else:
				print( "ValueErr 2")
				raise ValueError

		except ValueError as valErr:
			print('Value error in authorization_filter...' + str(valErr))

		except HTTPError as httpErr:
			print('HTTP error in authorization_filter...' + str(httpErr))

		except Exception as exc:
			print('in except: ' + str(exc))

		finally:
			print('Executed try except block')
			return auth_output


	@staticmethod
	def decode_token(index, token):
		print ("decoding.....")
		try:
			# These need to match the KEY_STRING_ARRAY in JWTUtil.java
			key_string_array = [
				"MFwwDQYJKoZIhvcNAQEBBQADSwAwSAJBALXFK/wna/hB6EwbrNST4Fk6V7O/IYm1wfGsq//9ua9tLxT4k+QMST+TLM/MoLMKTGcc6B3YZu4D7lhHxJmZp3sCAwEAAQ==",
				"MFwwDQYJKoZIhvcNAQEBBQADSwAwSAJBAIud7Ry9Wow908+++UqVe7pU3qvLAxMVddhb414Jrpi+Q2siVhXETRdaFOq1upsSFVbMpWTxhJtpL7FYIYPPjMsCAwEAAQ==",
				"MFwwDQYJKoZIhvcNAQEBBQADSwAwSAJBAKCHyITgm9GravdEyxnvGvaxupNEAzDgJXw4v/Qf6yU0zgaJ601YxYytEw5wA19hfgm5s2tVtA1FZtmTbbIi500CAwEAAQ==",
				"MFwwDQYJKoZIhvcNAQEBBQADSwAwSAJBANp+CbcKJmfdj4wkt1Fgt9FxCgqFWrUaMUieAfBWy2KvFaoD6Yzaae7rfeYouiF3QdtUtPyAeD+m0Ty69w52VW8CAwEAAQ==",
				"MFswDQYJKoZIhvcNAQEBBQADSgAwRwJAV3UumGSzOpikx6eQjCqu16sZNO9SbftUK1IxS4RZP3eq7hJLUVijgvbNs7xs4eVaWdCFsVnENp4qdmtgSOfWSwIDAQAB",
				"MFwwDQYJKoZIhvcNAQEBBQADSwAwSAJBAJhtiRKptnlAkhdstkrePo0X8sSBFipSRyLG6KfYm3cr67wgAHBZzhjOJKbRXqf3O3J8sUIIHKl5AVJftSYXZG0CAwEAAQ==",
				"MFswDQYJKoZIhvcNAQEBBQADSgAwRwJAZTVPNc3d8+btOvp8OaFjjR2alN+Zl4aRmj9bPHC5PvpUT4pFqMRwVD8UHrc1K62kFbo97i9fvuGgDMeZ9tCm3QIDAQAB",
				"MFwwDQYJKoZIhvcNAQEBBQADSwAwSAJBAJpsE7JAAhzuq7rgWLNSelQdmSGP6z4gjbd/ivYbSvW6sw3Q/90FElfVTkhqWH10/uyaGeWE8vELf+KAh96JWzsCAwEAAQ==",
				"MFwwDQYJKoZIhvcNAQEBBQADSwAwSAJBAKFNoDtkYz7Xadm3o5y47NjIDTISv0NprHY9HDDOL1qvyh0v8EGdqV7DBmdLBJeEO/EitFSJqaSku+AEZ8baOpcCAwEAAQ==",
				"MFswDQYJKoZIhvcNAQEBBQADSgAwRwJAZQniaWFfa0uoNyz6kmOD6e45DWUou5OsblZa0OJ/ZLcWufXGI4yx+gKGkMW8c4SUdK4QA+iTZJjC8HZkNL5uXwIDAQAB"
			]

			prefix = "OURSCHOOL"
			to_hash = prefix + index
			
			# Python hash() is not the same as hashCode() in Java
			# See
			# https://stackoverflow.com/questions/22845913/function-to-replicate-the-output-of-java-lang-string-hashcode-in-python-and-no
			hh = 0
			for c in to_hash:
				hh = int((((31 * hh + ord(c)) ^ 0x80000000) & 0xFFFFFFFF) - 0x80000000)
				
			if hh < 0:
				hh = -hh
			
			hh = hh % len(key_string_array)
			
			key = key_string_array[hh]
			key = base64.b64decode(key, b'+/')
			
			dict_decoded_token = jwt.decode(token, key, audience='Content mount tool', algorithms="HS512" )
			
			# Only enable this line temporarily for debugging
			#dict_decoded_token = jwt.decode(token, key, audience='Content mount tool', algorithms="HS512" , options={"verify_signature": False})

			return dict_decoded_token

		except jwt.DecodeError as decErr:
			print('DecodeError in decode_token...', str(decErr))
			return None
		finally:
			pass
		

	def authorize_app_user(self, dict_decoded_token):
		#print('In authorize_app_user')
		iss = dict_decoded_token['iss']

		try:
			if iss == self.ISS_OS_MONOLITH:
				
				# Check expiry time of token
				expiry =  dict_decoded_token['exp']
				expiry_check = ( time.time() < expiry )
				
				#print("iss verified")
				audience = dict_decoded_token['aud']
				
				gr_ou = dict_decoded_token['grantedRole']
				cluster_details = dict_decoded_token['clusterDetails']

				dict_tmp_auth = self.app_user_access_level(audience, cluster_details, gr_ou)
				auth_flag = dict_tmp_auth['authFlag']
				
				env = dict_decoded_token["env"]
				# Check that environment of this auth function matches the one that generated the token
				env_check = False
				acceptableEnvironments = self.ALLOWED_ENVS.split(',')
				if env in acceptableEnvironments:
					env_check = True
				else:
					env_check = False
					print("Deny because of wrong environment flag in token. Frontend may be pointed at wrong backend!!")

				dict_auth = dict()
				if auth_flag and env_check and expiry_check:
					dict_auth = {
						"authCode": hc.HttpConstants.HTTP_RESPONSE_OK,
						"appUserId": dict_tmp_auth['appUserId']
					}
				elif not auth_flag:
					print( "reject: authorizationToken problem")
					dict_auth = {
						"authCode": hc.HttpConstants.HTTP_RESPONSE_FORBIDDEN,
						"appUserId": dict_tmp_auth['appUserId']
					}
				elif not env_check:
					print( "reject: env")
					dict_auth = {
						"authCode": hc.HttpConstants.HTTP_RESPONSE_FORBIDDEN,
						"appUserId": dict_tmp_auth['appUserId']
					}
				elif not expiry_check:
					print( "reject: expiry date")
					dict_auth = {
						"authCode": hc.HttpConstants.HTTP_RESPONSE_FORBIDDEN,
						"appUserId": dict_tmp_auth['appUserId']
					}
				return dict_auth
			else:
				raise jwt.InvalidIssuerError

		except jwt.InvalidIssuerError as invIssErr:
			print('InvalidIssuerError in authorize_app_user...' + str(invIssErr))
			return obj_hc.HTTP_RESPONSE_UNAUTHORIZED

		except HTTPError as httpErr:
			print('In http except of authorize_app_user: ' + httpErr)
			return httpErr.code

		finally:
			pass

	def app_user_access_level(self, audience, cluster_details, granted_role):
		try:
			#print( "app_user_access_level  ")
			
			dict_auth = dict()
			role_id_for_audience = self.get_role_for_audience(audience)
			dict_granted_role = dict()
			
			#print( json.dumps(granted_role))
			#print( json.dumps(role_id_for_audience))

			for gr in granted_role:
				if gr['roleId'] == role_id_for_audience:
					dict_granted_role = gr
				
				# (For testing, can be removed later)
				# Here we are also consider Admins as having the permission to mount content
				# Admin
				#if gr['roleId'] == 1 :
				#	dict_granted_role = gr
				#	break
				

			if not emptyUtils.CheckEmptyUtils.is_empty(dict_granted_role):
				
				appUserOrgId = dict_granted_role['appUserOrgId']
				ouId = cluster_details['ouId']
				grantedRoleOrgUnitTypeId = dict_granted_role['grantedRoleOrgUnitTypeId']
				ouTypeId = cluster_details['ouTypeId']
				
				grantedRolOrgType_eq_ouType = ( grantedRoleOrgUnitTypeId == ouTypeId )
				appUserOrg_eq_ou = ( appUserOrgId == ouId )
				
				if grantedRolOrgType_eq_ouType and appUserOrg_eq_ou:
					#print( "Authorized" )
					dict_auth = {
						"authFlag": True,
						"appUserId": dict_granted_role['appUserId']
					}
				else:
					#print( "Not authorized,  grantedRoleOrgUnitTypeId != ouTypeId  OR  appUserOrgId != ouId" )
					
					if (grantedRolOrgType_eq_ouType == False):
						print ('unauthorized  !grantedRolOrgType_eq_ouType ')
						
					if (appUserOrg_eq_ou == False):
						print ( 'unauthorized !appUserOrg_eq_ou')					
					
					dict_auth = {
						"authFlag": False,
						"appUserId": dict_granted_role['appUserId']
					}
			else:
				print( "app_user_access_level  granted_role is empty ")
				raise ValueError

		except ValueError as valErr:
			print('Value error in app_user_access_level...' + str(valErr))

		finally:
			return dict_auth

	def get_role_for_audience(self, audience):
		try:
			if audience == self.AUD_CONTENT_MOUNT_TOOL:
				return self.ROLE_ID_CONTENT_MOUNT_TOOL_ACCESS
			else:
				raise ValueError

		except ValueError as valErr:
			print('Value error in getRoleForAudience...' + str(valErr))

		finally:
			pass




