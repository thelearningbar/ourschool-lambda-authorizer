from __future__ import print_function

import re
import authorizerMain as authMain
import os
obj_auth_main = authMain.AuthorizerMain()


def lambda_handler(event, context):
	"""Do not print the auth token unless absolutely necessary """
	
	print("Client token: " + event['authorizationToken'])
#    print("Method ARN: " + event['methodArn'])
	principal_id = "user"
	
	print("....Begin....event:")
	print(event)

	tmp = event['methodArn'].split(':')
	api_gateway_arn_tmp = tmp[5].split('/')
	aws_account_id = tmp[4]

	policy = AuthPolicy(principal_id, aws_account_id)
	policy.rest_api_id = api_gateway_arn_tmp[0]
	policy.region = tmp[3]
	#policy.stage = api_gateway_arn_tmp[1]

	output = obj_auth_main.entry(event)
	#print('output is the below-> ', output)

#    policy.deny_all_methods()
#    policy.allow_method(HttpVerb.GET, "/test")  
#    policy.allow_all_methods()

	if output == 'Allow':
		print('in allow')
		#policy.allow_all_methods()

		# remove whitespace and turn the list of ips into an actual array
		ipstring = os.environ.get('ALLOWED_IPS')
		ipstring = ipstring.replace(" ", "")
		ipsInitial = ipstring.split(',')
		
		# Reject any ips in the list which are not a valid ipv4
		# Any invalid IP in the policy can make the the policy not work for other users in the list, causing access denied.
		ips=[]
		for ip in ipsInitial:
			if is_valid_ip(ip):
				ips.append(ip)
			else:
				print("invalid ip in environment variable:", ip)
		
		# Create IP whitelist in policy
		conditions = {'IpAddress' :  {
    	   "aws:SourceIp": ips
		}}
        
        #Allow all methods, but with ip whitelist condition
		policy.allow_method_with_conditions(HttpVerb.ALL, "*", conditions)
		
		# Use a line similar to the one below if we want to be more granular with permissions
		#policy.allow_method(HttpVerb.GET, "/Test")
	else:
		print('in deny')
		policy.deny_all_methods()

	# Finally, build the policy
	auth_response = policy.build()

	# new! -- add additional key-value pairs associated with the authenticated principal
	# these are made available by APIGW like so: $context.authorizer.<key>
	# additional context is cached
	context = {
		'key': 'value',# $context.authorizer.key -> value
		'number': 1,
		'bool': True
	}
	# context['arr'] = ['foo'] <- this is invalid, APIGW will not accept it
	# context['obj'] = {'foo':'bar'} <- also invalid
	auth_response['context'] = context
	
	print('auth_response -> ', auth_response)

	return auth_response

# Check if an ipv4 is valid 
def is_valid_ip(ip):
    m = re.match(r"^(\d{1,3})\.(\d{1,3})\.(\d{1,3})\.(\d{1,3})$", ip)
    return bool(m) and all(map(lambda n: 0 <= int(n) <= 255, m.groups()))

def allowAll(event, context):
#    print("Client token: " + event['authorizationToken'])
	print("Method ARN: " + event['methodArn'])
	principal_id = "user"
	policy = AuthPolicy(principal_id, aws_account_id)
	policy.allow_all_methods()
	auth_response = policy.build()
	context = {
		'key': 'value',# $context.authorizer.key -> value
		'number': 1,
		'bool': True
	}
	auth_response['context'] = context
	print('auth_response -> ', auth_response)
	return auth_response


class HttpVerb:
	GET = "GET"
	POST = "POST"
	PUT = "PUT"
	PATCH = "PATCH"
	HEAD = "HEAD"
	DELETE = "DELETE"
	OPTIONS = "OPTIONS"
	ALL = "*"


class AuthPolicy(object):
	aws_account_id = ""
	"""The AWS account id the policy will be generated for. This is used to create the method ARNs."""
	principal_id = ""
	"""The principal used for the policy, this should be a unique identifier for the end user."""
	version = "2012-10-17"
	"""The policy version used for the evaluation. This should always be '2012-10-17'"""
	path_regex = "^[/.a-zA-Z0-9-\*]+$"
	"""The regular expression used to validate resource paths for the policy"""

	"""these are the internal lists of allowed and denied methods. These are lists
	of objects and each object has 2 properties: A resource ARN and a nullable
	conditions statement.
	the build method processes these lists and generates the approriate
	statements for the final policy"""
	allow_methods = []
	deny_methods = []

	rest_api_id = "*"
	"""The API Gateway API id. By default this is set to '*'"""
	region = "*"
	"""The region where the API is deployed. By default this is set to '*'"""
	stage = "*"
	"""The name of the stage used in the policy. By default this is set to '*'"""

	def __init__(self, principal, aws_account_id):
		self.aws_account_id = aws_account_id
		self.principal_id = principal
		self.allow_methods = []
		self.deny_methods = []

	def _add_method(self, effect, verb, resource, conditions):
		"""Adds a method to the internal lists of allowed or denied methods. Each object in
		the internal list contains a resource ARN and a condition statement. The condition
		statement can be null."""
		if verb != "*" and not hasattr(HttpVerb, verb):
			raise NameError("Invalid HTTP verb " + verb + ". Allowed verbs in HttpVerb class")
		resource_pattern = re.compile(self.path_regex)
		if not resource_pattern.match(resource):
			raise NameError("Invalid resource path: " + resource + ". Path should match " + self.path_regex)

		if resource[:1] == "/":
			resource = resource[1:]

		resource_arn = ("arn:aws:execute-api:" +
						self.region + ":" +
						self.aws_account_id + ":" +
						self.rest_api_id + "/" +
						self.stage + "/" +
						verb + "/" +
						resource)

		if effect.lower() == "allow":
			self.allow_methods.append({
				'resourceArn': resource_arn,
				'conditions': conditions
			})
		elif effect.lower() == "deny":
			self.deny_methods.append({
				'resourceArn': resource_arn,
				'conditions': conditions
			})

	def _get_empty_statement(self, effect):
		"""Returns an empty statement object prepopulated with the correct action and the
		desired effect."""
		statement = {
			'Action': 'execute-api:Invoke',
			'Effect': effect[:1].upper() + effect[1:].lower(),
			'Resource': []
		}

		return statement

	def _get_statement_for_effect(self, effect, methods):
		"""This function loops over an array of objects containing a resourceArn and
		conditions statement and generates the array of statements for the policy."""
		statements = []

		if len(methods) > 0:
			statement = self._get_empty_statement(effect)

			for curMethod in methods:
				if curMethod['conditions'] is None or len(curMethod['conditions']) == 0:
					statement['Resource'].append(curMethod['resourceArn'])
				else:
					conditionalStatement = self._get_empty_statement(effect)
					conditionalStatement['Resource'].append(curMethod['resourceArn'])
					conditionalStatement['Condition'] = curMethod['conditions']
					statements.append(conditionalStatement)

			statements.append(statement)

		return statements

	def allow_all_methods(self):
		"""Adds a '*' allow to the policy to authorize access to all methods of an API"""
		self._add_method("Allow", HttpVerb.ALL, "*", [])

	def deny_all_methods(self):
		"""Adds a '*' allow to the policy to deny access to all methods of an API"""
		self._add_method("Deny", HttpVerb.ALL, "*", [])

	def allow_method(self, verb, resource):
		"""Adds an API Gateway method (Http verb + Resource path) to the list of allowed
		methods for the policy"""
		self._add_method("Allow", verb, resource, [])

	def deny_method(self, verb, resource):
		"""Adds an API Gateway method (Http verb + Resource path) to the list of denied
		methods for the policy"""
		self._add_method("Deny", verb, resource, [])

	def allow_method_with_conditions(self, verb, resource, conditions):
		"""Adds an API Gateway method (Http verb + Resource path) to the list of allowed
		methods and includes a condition for the policy statement. More on AWS policy
		conditions here: http://docs.aws.amazon.com/IAM/latest/UserGuide/reference_policies_elements.html#Condition"""
		self._add_method("Allow", verb, resource, conditions)

	def deny_method_with_conditions(self, verb, resource, conditions):
		"""Adds an API Gateway method (Http verb + Resource path) to the list of denied
		methods and includes a condition for the policy statement. More on AWS policy
		conditions here: http://docs.aws.amazon.com/IAM/latest/UserGuide/reference_policies_elements.html#Condition"""
		self._add_method("Deny", verb, resource, conditions)

	def build(self):
		"""Generates the policy document based on the internal lists of allowed and denied
		conditions. This will generate a policy with two main statements for the effect:
		one statement for Allow and one statement for Deny.
		Methods that includes conditions will have their own statement in the policy."""
		if ((self.allow_methods is None or len(self.allow_methods) == 0) and
				(self.deny_methods is None or len(self.deny_methods) == 0)):
			raise NameError("No statements defined for the policy")

		policy = {
			'principalId' : self.principal_id,
			'policyDocument' : {
				'Version' : self.version,
				'Statement' : []
			}
		}

		policy['policyDocument']['Statement'].extend(self._get_statement_for_effect("Allow", self.allow_methods))
		policy['policyDocument']['Statement'].extend(self._get_statement_for_effect("Deny", self.deny_methods))

		return policy
